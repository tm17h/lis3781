# LIS3781 - Adcanced Database Management

## Thomas Martin

### Assignment 4 Requirements:

#### Assignment Screenshots:

*Screenshot of Assignment 4 ERD Part 1*:

![Assignment ERD Screenshot](img/A4ERD1.png)

*Screenshot of Assignment 4 ERD Part 2*:

![Assignment ERD Screenshot](img/A4ERD2.png)


#### Assignment Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/tm17h/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

[LIS3781 Bitbucket Repository](https://bitbucket.org/tm17h/lis3781/src/master/ "LIS3781 Repository")
