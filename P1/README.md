> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Thomas Martin

### Project 1 Requirements:

#### Assignment Screenshots:

*Screenshot of Project 1 ERD*:

![Project 1 ERD Screenshot](img/P1ERD.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/tm17h/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

[LIS3781 Bitbucket Repository](https://bitbucket.org/tm17h/lis3781/src/master/ "LIS3781 Repository")