> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Thomas Martin

#### Assignment 2 Screenshots:

*Screenshot of Project 2*:

![Project 2 Screenshot](img/P2_Screenshot.png)

#### Bitbucket Links:

[Bitbucket Station Locations Tutorial Link](https://bitbucket.org/tm17h/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

[LIS3781 Bitbucket Repository](https://bitbucket.org/tm17h/lis3781/src/master/ "LIS3781 Repository")
