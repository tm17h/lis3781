> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Thomas Martin

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    - Business RulesThe human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes:job description, length of employment, benefits,number ofdependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories mustbe tracked. Also, include the following business rules:
    - Each employee may have one or more dependents.
    - Each employee has only one job.
    - Each job can be held by many employees.
    - Many employees may receive many benefits.
    - Many benefitsmay be selected by many employees
    - Assignment 1 ERD

1. [A2 README.md](A2/README.md "My A2 README.md file")
    - Locally: Create yourfsuid Database & two tables: Company and Customer.
    - Screenshot of SQL Code
    - Screenshot of Populated Tables

1. [A3 README.md](A3/README.md "My A3 README.md file")
    - Create and Populate tables within Oracle SQL Developer
    - Screenshot of SQL Code
    - Screenshot of Populated Tables

1. [A4 README.md](A4/README.md "My A4 README.md file")
    - Business Rules:A high-volumehome office supply company contractsa database designer to develop a systemin orderto track its day-to-day business operations. The CFO needsan updated method for storing data,running reports, and makingbusiness decisions based upon trends and forecasts, as well as maintaininghistorical datadue to new governmental regulations.Here are the mandatory business rules:
    - • A sales representative has at least one customer, and each customer has at leastone sales repon any given day(as it is a high-volumeorganization).
    - • A customer places at least one order. However, each order is placed by only one customer.
    - • Each order contains at least one order line.Conversely, each order line is contained in exactly one order.
    - • Each product may be on a number of order lines. Though, each order line containsexactly one productid (though, each product idmay have a quantity of more than one included, e.g., “oln_qty”).
    - • Each order is billed on one invoice, and each invoice is a bill for exactly one order(by only one customer).
    - Screenshots of Assignment 4 ERD

1. [A5 README.md](A5/README.md "My A5 README.md file")
    - Business Rules: Expanding upon the high-volumehome office supply company’s data tracking requirements, the CFO requests your services again to extend the data model’s functionality. The CFO has read about the capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a smaller data mart as a test platform. He is under pressure from the members of the company’s board of directors who want to reviewmore detailed salesreports based upon the following measurements:
    - Product
    - Customer
    - Sales Representative
    - Time
    - Location
    - Furthermore, the board members want location to be expanded to include the following characteristics of location:
    - Region
    - State
    - City
    - Store

1. [P1 README.md](P1/README.md "My P1 README.md file")
    - Business Rules:As the lead DBA for a local municipality, you are contacted by the city council to design a database in order to track and document the city’scourt case data. Some report examples: Which attorney is assigned to what case(s)?How many unique clients have cases(be sureto add a client tomore than one case)?How many cases has each attorney been assigned, and names of their clients (return number and names)?How many cases does each client have with the firm(return a name and number value)?Which types ofcases does/dideach client have/hadand their start and end dates?Which attorney is associated to which client(s), and to which case(s)?Names of three judges with the most number of years in practice, includenumber of years.Also, include the following business rules:
    - An attorney is retainedby (or assigned to)one or moreclients,for each case.
    - A client has(or is assigned to) one or more attorneysfor each case.
    - An attorney hasone or more cases.
    - A client has one or more cases.
    - Each court has one or more judges adjudicating.
    - Each judge adjudicates upon exactly one court.
    - Each judgemaypresideovermore than one case.
    - Each case that goes to court ispresided over by exactly one judge.
    - A person can have more than one phone number.
    - Screenshot of Project 1 ERD

1. [P2 README.md](P2/README.md "My P2 README.md file")
    - Use the following links and tutorials, answer the following questions below:
    - How to Create Database & Collection in MongoDB
    - Add MongoDB Array using insert() with Example•Mongodb Primary Key: Example to set _id field with ObjectId() 
    - MongoDB Query Document using find() with Example
    - MongoDB Cursor Tutorial: Learn with Example
    - MongoDB order with Sort() & Limit() Query with Examples
    - MongoDB Count() & Remove() Functions with Examples
    - MongoDB Update() Document with Example
